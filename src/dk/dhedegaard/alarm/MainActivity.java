package dk.dhedegaard.alarm;

import android.app.Activity;
import android.content.Intent;
import android.provider.AlarmClock;

public class MainActivity extends Activity {

	@Override
	protected void onStart() {
		super.onStart();
		final Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM);
		startActivity(intent);
		finish();
	}

}